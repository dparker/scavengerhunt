﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.ServiceModel
Imports System.ServiceModel.Activation
Imports System.ServiceModel.Web
Imports System.Text
Imports System.Net
Imports System.IO
Imports System.Net.Mail
<ServiceContract()>
<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)>
<ServiceBehavior(InstanceContextMode:=InstanceContextMode.PerCall)>
Public Class Scavenger
    <WebGet(UriTemplate:="Ready", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json)>
    Public Function Ready() As ReadyData
        Ready = New ReadyData
        Dim ds As DataSet = GeneralDataSet("select top 1 ready,secret from ready")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("Ready") Then
                Ready.Ready = "1"
            Else
                Ready.Ready = "0"
            End If
            Ready.Secret = ds.Tables(0).Rows(0).Item("secret").ToString
        Else
            Ready.Ready = "0"
        End If
    End Function
    <WebGet(UriTemplate:="Teams", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json)>
    Public Function Teams() As List(Of Team)
        Teams = New List(Of Team)
        Dim ds As DataSet = GeneralDataSet("select id,name,color from teams")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            For Each r As DataRow In ds.Tables(0).Rows
                If r.Item("name").ToString <> "" Then Teams.Add(New Team With {.Color = r.Item("color").ToString, .ID = r.Item("id").ToString, .Name = r.Item("name").ToString})
            Next
        End If
        Return Teams
    End Function
    <WebGet(UriTemplate:="Clues/{Team}", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json)>
    Public Function Clues(Team As String) As ClueData
        Clues = New ClueData
        Clues.Clues = New List(Of Clue)
        Dim iCounter As Integer = 0
        Dim iCounter2 As Integer = 1
        Dim ds As DataSet = GeneralDataSet("select id,clue,(select count(*) from images where teamid=" + Team + " and clueid=clues.id) as completed from clues order by Order" + Team)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            For Each r As DataRow In ds.Tables(0).Rows
                If r.Item("clue").ToString <> "" Then
                    Clues.Clues.Add(New Clue With {.Clue = r.Item("clue").ToString, .ID = r.Item("id").ToString, .Completed = r.Item("completed").ToString, .ClueNumber = "Clue to location " + iCounter2.ToString})
                    If r.Item("completed") > 0 Then iCounter = iCounter + 1
                    iCounter2 = iCounter2 + 1
                End If
            Next
        End If
        Clues.NumberCompleted = iCounter
        Clues.TotalClues = iCounter2 - 1
        Return Clues
    End Function
    Public Class ClueData
        Property NumberCompleted
        Property TotalClues
        Property Clues As List(Of Clue)
    End Class
    Public Class Clue
        Property ClueNumber
        Property ID
        Property Clue
        Property Completed
        Property Image
    End Class
    Public Class ReadyData
        Property Ready
        Property Secret
    End Class
    Public Class Team
        Property ID
        Property Name
        Property Color
    End Class
End Class